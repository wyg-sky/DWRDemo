#! /usr/bin/env bash

# 需要配置如下参数
# 项目路径，在Execute Shell中配置项目路径， pwd 就可以获得该项目路径
# export PROJ_PATH=这个Jenkins任务在部署机器上的路径

# 输入你的服务器环境上Tomcat的全路径
# export TOMCAT_APP_PATH=tomcat在部署机器上的全路径

### base 函数
killTomcat()
{
    pid=`ps -ef|grep tomcat|grep java|awk '{print $2}'`
    echo "tomcat Id list :$pid"
    if [ "$pid" = "" ]
    then
        echo "no tomcat pid alive"
    else
        kill -9 $pid
    fi
}
cd $PROJ_PATH/DWRDemo
mvn clean install

# 停Tomcat
killTomcat

# 删除原有工程
rm -rf $TOMCAT_APP_PATH/webapps/ROOT
rm -r $TOMCAT_APP_PATH/webapps/ROOT.war
rm -r $TOMCAT_APP_PATH/webapps/DWRDemo-1.0.war

# 复制新的工程
cp $PROJ_PATH/DWRDemo/target/DWRDemo-1.0.war $TOMCAT_APP_PATH/webapps/

cd $TOMCAT_APP_PATH/webapps/
mv DWRDemo-1.0.war ROOT.war

# 启动Tomcat
cd $TOMCAT_APP_PATH/
sh bin/startup.sh