package hytalk.bean;
/**
 * @description ：DWR测试
 * @date ：2017年3月12日 上午4:36:58
 * @author ：WangYG
 */
public class DwrTest {

    public String sendMsg(String message){
        System.out.println(" Message : "+ message);
        return "Hello : "+ message;
    }
}
