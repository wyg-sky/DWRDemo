<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>DWR消息推送测试页面</title>
<!-- 注意以下两个javascript的顺序不能随便改   
    1.第一个是engine.js文件，由dwr.jar包提供  
    2.第二个是根据自己写的类生成的js文件，格式为/{app name}/dwr/interface/{在dwr中暴露的javascript类名.js}  
    3.如果对script的写法不好掌握，可以调用以下URL查看:  
        http://urlname/app name/drw/  
    -->  
    <script type='text/javascript' src='/DWRDemo/dwr/engine.js'></script>  
    <script type='text/javascript' src='/DWRDemo/dwr/interface/DwrTest.js'></script>  
    <script type='text/javascript' src='/DWRDemo/dwr/util.js'></script>
      
          
    <script type="text/javascript">  
  
        function sendMessage()  
        {  
            var message = document.getElementById("message").value;  
            //alert(message);  
            //直接用dwr.xml中暴露出来的类来调用，第一个是方法test的传入参数，最后一个是回调的方法  
            DwrTest.sendMsg(message,showMessage);  
        }  
        //回调方法  
        function showMessage(data)  
        {
        	//alert(data);
        	var div = document.getElementById("returnmessage");
        	div.innerHTML = div.innerHTML +data+'<br>';
        }  
  
        </script>  
</head>  
<body>  
    <input type="text" id="message" name="message" />  
    <input type="button" value="send message" onclick = "sendMessage()" />  
    <div id="returnmessage"></div>   
</body>  
</html>